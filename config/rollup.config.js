const baseConfig = {
  input: 'index.js',
  output: {
    name: 'microcheck',
    file: 'dist/index.js',
    format: 'es',
  },
};

export default [
  baseConfig,
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.umd.js',
      format: 'umd',
    }
  },
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.iife.js',
      format: 'iife',
    }
  },
  {
    ...baseConfig,
    output: {
      ...baseConfig.output,
      file: 'dist/index.cjs.js',
      format: 'cjs',
    }
  }
];