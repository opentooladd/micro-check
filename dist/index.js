import difference from 'microutils.js/dist/array/difference.umd.js';

/*
Copyright 2018 Ciro DE CARO

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

const nRegEx = new RegExp(/[\-\+]{0,1}\d+(\.\d+)?/, 'g');
const maxRegEx = new RegExp(/^>\d+(\.\d+)?$/);
const minRegEx = new RegExp(/^<\d+(\.\d+)?$/);
const rangeRegEx = new RegExp(/^\d+(\.\d+)?<>\d+(\.\d+)?$/);

const isNotNativeFunction = (input) => {
  return !input.toString().includes('function Function() { [native code] }')
        && input !== Array
        && input !== Function
        && input !== Object
        && input !== String
        && input !== Number;
};

const isContructor = (f) => {
  try {
    new f();
  } catch (err) {
    if (err.message.indexOf('is not a constructor') >= 0) {
      return false;
    }
  }
  return true;
};

var index = ({
  validate(input, schema, strict = false) {
    const keys = Object.keys(schema);
    const notVarKeys = keys.filter(k => !k.includes('?'));
    const inputKeys = Object.keys(input);
    const keyDifference = difference(notVarKeys, inputKeys);

    if (keyDifference.length > 0) return this.logError({
      input,
      source: keys,
      valid: false,
      message: `ValidationError: ${JSON.stringify(input)} missing keys [${keyDifference}]`
    });

    inputKeys.forEach(key => {
      const source = (schema[key] || schema[`?${key}`]);
      if (source) this.check(input[key], source);
      else if (!source && strict) return this.checkArray(inputKeys, keys);
    });

    return null;
  },
  check(input, source) {
    if (typeof source !== 'undefined' && source !== null) {
      if (source instanceof Function) {
        if (isNotNativeFunction(source)) {
          if (isContructor(source)) return this.logError({
            input,
            source,
            valid: input instanceof source,
            message: `ValidationError: ${JSON.stringify(input)} is not an instanceof ${source.name}`,
          }); 
          const result = source(input);
          return this.logError({
            input,
            source,
            valid: result,
            message: `ValidationError: ${JSON.stringify(input)} did not pass ${source.toString()}: returned ${result}`,
          });
        }
        return this.checkType(input, source);
      }
      const numberTest = (maxRegEx.test(source) || minRegEx.test(source) || rangeRegEx.test(source)) || this.getType(source) === 'Number';
      const stringTest = typeof source === 'string' || source instanceof RegExp || this.getType(source) === 'String';
      const arrayTest = source instanceof Array;

      if (numberTest) {
        return this.checkNumber(input, source);
      } else if (stringTest) {
        return this.checkString(input, source);
      } else if (arrayTest) {
        const typeArray = source.filter(o => o instanceof Function);
        if (typeArray.length > 0 && typeArray.length === source.length) {
          let success = false;
          for (let i = 0; i < typeArray.length; i++) {
            try {
              success = true;
              this.checkType(input, typeArray[i]);
              break;
            } catch (e) {
              success = false;
            }
          }
          if (!success) {
            return this.logError({
              input,
              source,
              valid: false,
              message: `${input} should be of type ${typeArray.map(a => this.getType(a))}`
            });
          }
          return null;
        } else {
          return this.checkArray(input, source);
        }
      } else {
        return this.checkObject(input, source);
      }
    }
    return null;
  },
  checkType(input, source) {
    const t = input ? `${(this.getType(source)[0]).toLowerCase()}${this.getType(source).slice(1)}` : true;
    const tInput = input ? `${(input.constructor.name[0]).toLowerCase()}${input.constructor.name.slice(1)}` : false;
    if (t !== tInput) {
      return this.logError({
        input,
        source,
        valid: false,
        message: `${JSON.stringify(input)} should be of type ${this.getType(source)}`
      });
    }
    return null;
  },
  checkString(input, source) {
    if (source instanceof RegExp) {
      return this.logError({
        input,
        source,
        valid: source.test(input),
      });
    } else {
      return this.logError({
        input,
        source,
        valid: source === input,
      });
    }
  },
  checkNumber(input, source) {
    const minTest = minRegEx.test(source);
    const maxTest = maxRegEx.test(source);
    const rangeTest = rangeRegEx.test(source);
    if (rangeTest && !minTest && !maxTest) {
      const matchMin = source.match(nRegEx)[0];
      const min = parseFloat(matchMin);
      const matchMax = source.match(nRegEx)[1];
      const max = parseFloat(matchMax);
      return this.logError({
        input,
        source,
        valid: input < max && input > min,
      });
    } else if (maxTest && !minTest && !rangeTest) {
      const match = source.match(nRegEx)[0];
      const n = parseFloat(match);
      return this.logError({
        input,
        source,
        valid: input > n,
      });
    } else {
      const match = source.match(nRegEx)[0];
      const n = parseFloat(match);
      return this.logError({
        input,
        source,
        valid: input !== '' && typeof input !== 'undefined' && input !== null && input < n,
      });
    }
  },
  checkArray(input, source) {
    const diffs = difference(source, input);
    return this.logError({
      input: JSON.stringify(input),
      source: JSON.stringify(source),
      valid: diffs.length === 0,
    });
  },
  checkObject(input, source) {
    return this.validate(input, source);
  },
  logError(error) {
    const message = `ValidationError: ${error.input} type:${typeof error.input} must respect ${error.source} type:${typeof error.source}`;
    if (!error.valid) {
      throw new TypeError(error.message || message);
    }
    return null;
  },
  getType(source) {
    return source.name || typeof source;
  }
});

export { index as default };
